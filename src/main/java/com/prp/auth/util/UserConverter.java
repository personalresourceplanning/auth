/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.util;

import java.util.HashSet;
import java.util.Set;

import com.prp.auth.dal.model.UserDB;
import com.prp.auth.dal.model.UserPermissionsDB;
import com.prp.auth.pojo.User;
import com.prp.auth.security.enums.UserPermissions;

/**
 * Util for converting {@link User}, {@link UserDB}, {@link UserPermissions} and {@link UserPermissionsDB} objects.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see User
 * @see UserPermissions
 * @see UserDB
 * @see UserPermissionsDB
 *
 */
public class UserConverter {

	private UserConverter() {}

	public static UserDB convertUser(final User user) {
		UserDB retVal = new UserDB();
		retVal.setActive(user.getActivationState());
		retVal.setEmail(user.getPrimaryEmail());
		retVal.setPassword(user.getPassword());
		return retVal;
	}

	public static User convertUser(final UserDB user) {
		User retVal = new User();
		retVal.setActivationState(user.getActive());
		retVal.setPrimaryEmail(user.getEmail());
		retVal.setPassword(user.getPassword());
		retVal.setId(user.getId());
		return retVal;
	}

	public static Set<UserPermissionsDB> convertPermissions(final Set<UserPermissions> permissions) {
		Set<UserPermissionsDB> dbSet = new HashSet<>();
		for (UserPermissions permission : permissions) {
			UserPermissionsDB db = new UserPermissionsDB();
			db.setPermission(permission);
		}
		return dbSet;
	}

}
