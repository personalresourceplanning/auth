/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.service.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.prp.auth.pojo.User;
import com.prp.auth.security.enums.UserActivationState;
import com.prp.auth.security.enums.UserPermissions;

import lombok.Getter;

public class UserDetailsImpl implements UserDetails {

	private static final long serialVersionUID = -7185997538152291345L;

	@Getter
	private User user;

	public UserDetailsImpl(final User user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthorityImpl> grantedAuthorities = new HashSet<>();
		for (UserPermissions permission : user.getPermissions()) {
			GrantedAuthorityImpl ga = new GrantedAuthorityImpl(permission);
			grantedAuthorities.add(ga);
		}
		return grantedAuthorities;
	}

	@Override
	public String getUsername() {
		return user.getPrimaryEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !user.getActivationState().equals(UserActivationState.LOCKED);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return !user.getActivationState().equals(UserActivationState.WAITING_FOR_ACTIVATION);
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

}
