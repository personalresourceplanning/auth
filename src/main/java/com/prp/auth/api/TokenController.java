/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

	private static final Logger log = LoggerFactory.getLogger(TokenController.class);

	private ConsumerTokenServices tokenServices;

	@Autowired
	public TokenController(final ConsumerTokenServices tokenServices) {
		this.tokenServices = tokenServices;
	}

	@GetMapping("/oauth/revoke/{tokenId}")
	@ResponseBody
	// TODO not working right now
	public String revokeToken(@PathVariable(name = "tokenId", required = true) final String tokenId) {
		tokenServices.revokeToken(tokenId);
		log.debug("Revoked token " + tokenId);
		return tokenId;
	}

}
