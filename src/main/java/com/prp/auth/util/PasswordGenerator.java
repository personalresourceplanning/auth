/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * Class to generate passwords and registration keys.
 * <p>
 * Taken from <a href= "https://stackoverflow.com/questions/19743124/java-password-generator">Stackoverflow</a>
 *
 * @author George Siggouroglou
 *
 */
public class PasswordGenerator {

	private static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
	private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String DIGITS = "0123456789";
	private static final String PUNCTUATION = "!@#$%&*()_+-=[]|,./?><";
	private boolean useLower;
	private boolean useUpper;
	private boolean useDigits;
	private boolean usePunctuation;

	private PasswordGenerator() {
		throw new UnsupportedOperationException("Empty constructor is not supported.");
	}

	private PasswordGenerator(final PasswordGeneratorBuilder builder) {
		this.useLower = builder.useLower;
		this.useUpper = builder.useUpper;
		this.useDigits = builder.useDigits;
		this.usePunctuation = builder.usePunctuation;
	}

	public static class PasswordGeneratorBuilder {

		private boolean useLower;
		private boolean useUpper;
		private boolean useDigits;
		private boolean usePunctuation;

		public PasswordGeneratorBuilder() {
			this.useLower = false;
			this.useUpper = false;
			this.useDigits = false;
			this.usePunctuation = false;
		}

		/**
		 * Set true in case you would like to include lower characters (abc...xyz). Default false.
		 *
		 * @param useLower true in case you would like to include lower characters (abc...xyz). Default false.
		 * @return the builder for chaining.
		 */
		public PasswordGeneratorBuilder useLower(final boolean useLower) {
			this.useLower = useLower;
			return this;
		}

		/**
		 * Set true in case you would like to include upper characters (ABC...XYZ). Default false.
		 *
		 * @param useUpper true in case you would like to include upper characters (ABC...XYZ). Default false.
		 * @return the builder for chaining.
		 */
		public PasswordGeneratorBuilder useUpper(final boolean useUpper) {
			this.useUpper = useUpper;
			return this;
		}

		/**
		 * Set true in case you would like to include digit characters (123..). Default false.
		 *
		 * @param useDigits true in case you would like to include digit characters (123..). Default false.
		 * @return the builder for chaining.
		 */
		public PasswordGeneratorBuilder useDigits(final boolean useDigits) {
			this.useDigits = useDigits;
			return this;
		}

		/**
		 * Set true in case you would like to include punctuation characters (!@#..). Default false.
		 *
		 * @param usePunctuation true in case you would like to include punctuation characters (!@#..). Default false.
		 * @return the builder for chaining.
		 */
		public PasswordGeneratorBuilder usePunctuation(final boolean usePunctuation) {
			this.usePunctuation = usePunctuation;
			return this;
		}

		/**
		 * Get an object to use.
		 *
		 * @return the {@link gr.idrymavmela.business.lib.PasswordGenerator} object.
		 */
		public PasswordGenerator build() {
			return new PasswordGenerator(this);
		}
	}

	/**
	 * This method will generate a password depending the use* properties you define. It will use the categories with a probability. It is
	 * not sure that all of the defined categories will be used.
	 *
	 * @param length the length of the password you would like to generate.
	 * @return a password that uses the categories you define when constructing the object with a probability.
	 */
	public String generate(final int length) {
		// Argument Validation.
		if (length <= 0) {
			return "";
		}

		// Variables.
		StringBuilder password = new StringBuilder(length);
		Random random = new Random(System.nanoTime());

		// Collect the categories to use.
		List<String> charCategories = new ArrayList<>(4);
		if (useLower) {
			charCategories.add(LOWER);
		}
		if (useUpper) {
			charCategories.add(UPPER);
		}
		if (useDigits) {
			charCategories.add(DIGITS);
		}
		if (usePunctuation) {
			charCategories.add(PUNCTUATION);
		}

		// Build the password.
		for (int i = 0; i < length; i++) {
			String charCategory = charCategories.get(random.nextInt(charCategories.size()));
			int position = random.nextInt(charCategory.length());
			password.append(charCategory.charAt(position));
		}
		return new String(password);
	}
}
