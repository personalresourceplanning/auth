/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.dal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * Model for confirmation codes.
 *
 * @author Eric Fischer
 * @see UserDB
 *
 */
@Entity
@Table(name = "prp_authentication_confirmationcode")
public class ConfirmationCodeDB {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "user", unique = true, nullable = false)
	@Getter
	@Setter
	private Long userId;

	@Column(name = "activationcode", unique = true, nullable = false)
	@Getter
	@Setter
	private String confirmationCode;

}
