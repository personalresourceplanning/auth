/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.dal.impl;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.auth.dal.UserDal;
import com.prp.auth.dal.model.ConfirmationCodeDB;
import com.prp.auth.dal.model.UserDB;
import com.prp.auth.dal.model.UserPermissionsDB;
import com.prp.auth.dal.repository.ConfirmationCodeRepository;
import com.prp.auth.dal.repository.PermissionsRepository;
import com.prp.auth.dal.repository.UsersRepository;
import com.prp.auth.exception.PRPErrorCode;
import com.prp.auth.security.enums.UserActivationState;
import com.prp.auth.exception.PRPException;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see UserDal
 *
 */
@Component
public class UserDalImpl implements UserDal {

	private static final Logger log = LoggerFactory.getLogger(UserDalImpl.class);

	private UsersRepository usersRepository;
	private PermissionsRepository permissionsRepository;
	private ConfirmationCodeRepository confirmationCodeRepository;

	@Autowired
	public UserDalImpl(final UsersRepository usersRepository, final PermissionsRepository permissionsRepository,
			final ConfirmationCodeRepository confirmationCodeRepository) {
		this.usersRepository = usersRepository;
		this.permissionsRepository = permissionsRepository;
		this.confirmationCodeRepository = confirmationCodeRepository;
	}

	@Override
	public UserDB loadUser(final String email) throws PRPException {
		return usersRepository.findByEmail(email);
	}

	@Override
	public UserDB storeUser(final UserDB user, final Set<UserPermissionsDB> permissions) throws PRPException {

		if (user.getEmail() == null || user.getEmail().isEmpty() || user.getActive() == null
				|| user.getPassword() == null || user.getPassword().isEmpty()) {
			String message = "Not all needed information set to store the user into the database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_NOT_ALL_ATTRIBUTES_GIVEN);
		}

		if (usersRepository.findByEmail(user.getEmail()) != null) {
			String message = "There is an user already registered with this email.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_USER_ALREADY_REGISTERED);
		}

		if (!user.getActive().equals(UserActivationState.WAITING_FOR_ACTIVATION)) {
			log.warn("User activation sate not set to " + UserActivationState.WAITING_FOR_ACTIVATION.toString()
					+ ". Setting to this value.");
			user.setActive(UserActivationState.WAITING_FOR_ACTIVATION);
		}

		usersRepository.save(user);

		// save permissions
		UserDB loaded = usersRepository.findByEmail(user.getEmail());
		for (UserPermissionsDB permission : permissions) {
			permission.setUser(loaded);
			permissionsRepository.save(permission);
			log.debug("Stored permission for user.");
		}

		log.info("User stored to database.");

		return usersRepository.findByEmail(user.getEmail());
	}

	@Override
	public UserDB updateUser(final UserDB user) throws PRPException {

		if (user.getId() == null || user.getId().equals(Long.valueOf(0))) {
			String message = "Id not set to update the user into the database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_NOT_ALL_ATTRIBUTES_GIVEN);
		}

		if (usersRepository.findByEmail(user.getEmail()) == null) {
			String message = "There is no user in the database with this email.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_USER_NOT_FOUND);
		}

		usersRepository.save(user);

		return usersRepository.findByEmail(user.getEmail());
	}

	@Override
	public void storeConfirmationCode(final UserDB user, final String confirmationCode) throws PRPException {

		if (confirmationCode == null || confirmationCode.isEmpty()) {
			String message = "No confirmation code given for storing in database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_MISSING);
		}

		if (confirmationCode.length() < 8) {
			String message = "The given confirmation code has not the required minimal length.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_TOO_SHORT);
		}

		if (user == null || user.getId() == null || user.getId().equals(Long.valueOf(0))) {
			String message = "Required user parameters missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_USER_PARAMETER_MISSING);
		}

		if (confirmationCodeRepository.findByConfirmationCode(confirmationCode) != null) {
			String message = "Code already stored. A confirmation code cannot be stored twice.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_ALREADY_STORED);
		}

		ConfirmationCodeDB db = new ConfirmationCodeDB();
		db.setUserId(user.getId());
		db.setConfirmationCode(confirmationCode);
		confirmationCodeRepository.save(db);
	}

	@Override
	public String loadConfirmationCode(final UserDB user) throws PRPException {

		if (user == null || user.getId() == null || user.getId().equals(Long.valueOf(0))) {
			String message = "Required user parameters missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_USER_PARAMETER_MISSING);
		}

		ConfirmationCodeDB db = confirmationCodeRepository.findByUserId(user.getId());
		if (db == null) {
			return null;
		}

		return db.getConfirmationCode();
	}

	@Override
	public UserDB findUserForConfirmationCode(final String confirmationCode) throws PRPException {

		if (confirmationCode == null || confirmationCode.isEmpty()) {
			String message = "No confirmation code given for searching in database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_MISSING);
		}

		ConfirmationCodeDB db = confirmationCodeRepository.findByConfirmationCode(confirmationCode);
		if (db == null) {
			log.warn("No entry for confirmation code found.");
			return null;
		}

		Optional<UserDB> user = usersRepository.findById(db.getUserId());
		if (!user.isPresent()) {
			String message = "The user for the given ID does not exist.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_USER_DOES_NOT_EXIST);
		}

		return user.get();
	}

	@Override
	@Transactional
	public void deleteConfirmationCode(final String confirmationCode) throws PRPException {
		if (confirmationCode == null || confirmationCode.isEmpty()) {
			String message = "No confirmation code given for searching in database.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_DAL_CONFIRMATIONCODE_MISSING);
		}

		confirmationCodeRepository.deleteByConfirmationCode(confirmationCode);
	}

}
