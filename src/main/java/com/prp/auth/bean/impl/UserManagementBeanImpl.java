/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.bean.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.auth.bean.UserManagementBean;
import com.prp.auth.dal.UserDal;
import com.prp.auth.dal.model.UserDB;
import com.prp.auth.pojo.User;
import com.prp.auth.util.UserConverter;
import com.prp.auth.exception.PRPErrorCode;
import com.prp.auth.exception.PRPException;

/**
 * @since 0.1
 * @see UserManagementBean
 * @author Eric Fischer
 *
 */
@Component
public class UserManagementBeanImpl implements UserManagementBean {

	private static final Logger log = LoggerFactory.getLogger(UserManagementBeanImpl.class);

	private UserDal userDal;

	@Autowired
	public UserManagementBeanImpl(final UserDal userDal) {
		this.userDal = userDal;
	}

	@Override
	public User loadUser(final String primaryEmail) throws PRPException {
		if (primaryEmail == null || primaryEmail.isEmpty()) {
			String message = "Please provide an email address to search for an user.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_BEAN_EMAIL_MISSING);
		}

		UserDB db = userDal.loadUser(primaryEmail);

		if (db == null) {
			log.warn("No user found for input email.");
			return null;
		}

		User user = UserConverter.convertUser(db);

		return user;
	}

	@Override
	public User updateUser(final User user) throws PRPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteUserData(final User user) throws PRPException {
		// TODO Auto-generated method stub

	}

}
