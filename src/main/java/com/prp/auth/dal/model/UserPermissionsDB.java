/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.dal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.prp.auth.security.enums.UserPermissions;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Model for managing user permissions.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see UserPermissions
 * @see UserDB
 *
 */
@Entity
@Table(name = "prp_authentication_userpermissions")
@EqualsAndHashCode
public class UserPermissionsDB implements Serializable {

	private static final long serialVersionUID = -2512904493609375641L;

	@Id
	@GeneratedValue
	@Getter
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	@Getter
	@Setter
	private UserDB user;
	@Getter
	@Setter
	@Column(name = "permission", nullable = false)
	private UserPermissions permission;
}
