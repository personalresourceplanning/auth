/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.service.impl;

import org.springframework.stereotype.Service;

import com.prp.auth.service.EmailService;
import com.prp.auth.exception.PRPException;

@Service
public class EmailServiceImpl implements EmailService {

	@Override
	public void sendMail(final String receipient, final String confirmationCode) throws PRPException {
		// TODO Auto-generated method stub

	}

}
